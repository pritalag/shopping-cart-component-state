import React, { Component } from 'react';

import ProductList from './components/ProductList';
import Cart from './components/Cart'
import axios from 'axios';

class App extends Component {

  state = {
    products : [],
    cartItems:[],
    total: 0.0
  }

  componentDidMount() {
    axios
      .get('http://www.mocky.io/v2/5e5b00ab3000002a00f9f1aa')
      .then(resp => {
        this.setState({ ...this.state, products: resp.data.products})
      }); 
    
    }

  removeFromCart = (id,e) => {
    e.preventDefault();
    var item = this.state.cartItems.find(product => product.id === id);

    if(parseInt(item.count) > 1){
    item.count = parseInt(item.count)-1;
    }
    else{
      var index =  this.state.cartItems.indexOf(item);
      this.setState({...this.state, cartItems : this.state.cartItems.splice(index,1)});
    }

    this.minusTotal(this.state.total,item.price);
    
  }

  addToCart = (id,e) => {
    e.preventDefault();
    var itemexists = this.state.cartItems.some(item => item.id === id);
    var item = this.state.products.find(product => product.id === id);

    if(!itemexists){   
    var newItem = {...item,count : 1};  
      this.setState({...this.state, 
        cartItems : this.state.cartItems.push(newItem)    
      });
      item = newItem;   
    }
    else{
      this.setState({...this.state, cartItems : this.state.cartItems.map(cartItem => {
        if(cartItem.id==id){
          cartItem.count = parseInt(cartItem.count)+1;
          item = cartItem;
        }})
      });
    }
    this.addTotal(this.state.total,item.price);

  }

  formatdecimal = value => parseFloat(value).toFixed(2);

  addTotal = (total,itemPrice) => {
    var totalPrice =  (parseFloat(total) + parseFloat(itemPrice)).toFixed(2);
    this.setState({...this.state, total: totalPrice})
  }

  minusTotal = (total,itemPrice) => {
    var totalPrice =  (parseFloat(total) - parseFloat(itemPrice)).toFixed(2);
    this.setState({...this.state, total: totalPrice})  
  }


  render() {
  return (
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <h1>Online Shopping</h1>
            </div>
        </div>
        <div className="row">
            <div className="col-md-8">
                <ProductList products={this.state.products}
                             addToCart={this.addToCart} 
                             isInCart={false} />
            </div>
            <div className="col-md-4">
                <Cart items={this.state.cartItems} 
                      removeFromCart={this.removeFromCart} total={Number(this.state.total).toFixed(2)}/>
            </div>
        </div>
    </div>
);
  }
}

export default App;
