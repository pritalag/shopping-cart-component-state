import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CartItem from './CartItem';

class Cart extends Component {

   render() {
        const {items, removeFromCart , total} = this.props 
        const currency = 'EUR';

        return (
            <div>
                <h3>Shopping Cart</h3>

                <div className="cart">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            {items.length > 0 && (
                                <div className="cart__body">
                                    {items.map(item => 
                                            (<CartItem key={item.id} {...item} onClick={(e) => removeFromCart(item.id,e)} />)
                                        )}
                                </div>
                            )}
                            {items.length === 0 && (
                                <div className="alert alert-info">Cart is empty</div>
                            )}
                            <div className="cart__total">Total: {total} {currency}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Cart.propTypes = {
    items: PropTypes.array,
    total: PropTypes.string,
    currency: PropTypes.string
}

export default Cart;
