import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CartItem extends Component  {
    render(){
        const { name, price, currency,count, onClick } = this.props;
    return (
        <div className="cart-item">
            <div>
                <button className="btn btn-danger btn-xs" onClick={onClick}>X</button>
                <span className="cart-item__name">{name} x {count}</span>
            </div>
            <div className="cart-item__price">{price * count} {currency}</div>
        </div>
    )};
}

CartItem.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

export default CartItem;
