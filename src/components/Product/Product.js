import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Product extends Component {
    render() {
        const { id,name, price, currency, image, addToCart } = this.props;

        return (
            <div className="product thumbnail">
                <img src={image} alt="product" />
                <div className="caption">
                    <h3>{name}</h3>
                    <div className="product__price">{price} {currency}</div>
                    <div className="product__button-wrap">
                        <button
                            className='btn btn-primary'
                            onClick={(e) => addToCart(id,e)}
                        >
                            Add to cart
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

Product.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number,
    currency: PropTypes.string,
    image: PropTypes.string,
    addToCart: PropTypes.func.isRequired
}

export default Product;
