import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Product from './../Product';

class ProductList extends Component {
    render() {
        const { products,addToCart } = this.props;

        return (
            <div>
                <h3>Products</h3>
                <ul className="product-list">
                  {products.map(product => (
                      <li key={product.id} className=".product-list__item">
                            <Product key={product.id} {...product} addToCart={addToCart}/>
                                                  
                      </li>
                  ))}
                </ul>
            </div>
        );
    }
}

ProductList.propTypes = {
    products: PropTypes.array,
}

export default ProductList;
