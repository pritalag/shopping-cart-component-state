import {Product} from './Product';
import { storiesOf } from '@storybook/react';
import {checkA11y} from '@storybook/addon-a11y';
import {withKnobs} from '@storybook/addon-knobs';

const products = [{
    "id": 1,
    "name": "AIWO 737A1 Laptop",
    "price": 699.99,
    "currency": "EUR",
    "image": "https://www.gizmochina.com/wp-content/uploads/2018/08/AIWO-737A1-Laptop-400x400.jpg"
    },
    {
    "id": 2,
    "name": "Huawei Matebook 13 Laptop",
    "price": 549.99,
    "currency": "EUR",
    "image": "https://www.priceboon.com/wp-content/uploads/2019/01/20181220094634_19726-400x400.jpg"
    }];


const stories = storiesOf('Components/Product', module)

stories.addDecorator(withKnobs);

